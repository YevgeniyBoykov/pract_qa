package projectPack.testBase;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Set;
import java.util.concurrent.TimeUnit;


public class TestBase {
    private WebDriver driver;
    private static String appURL = "https://skyup.aero/ru/";
    private static String linuxPathDriver = "//home//jen//IdeaProjects//chromedriver";
    private static String WinPathDriver = "C:\\Webdriver\\chromedriver.exe";

    public WebDriver getDriver() {
        return driver;
    }

    public static WebDriver setChromeDriver() {
        System.setProperty("webdriver.chrome.driver", WinPathDriver);
        WebDriver driver = new ChromeDriver();
        //driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.navigate().to(appURL);
        driver.findElement(By.xpath("//div[@id='cookieModal']/div/div[2]/button")).click();

        driver.findElement(By.id("deprtureCity")).click();
        WebElement elementFrom = driver.findElement(By.id("citiesSearch"));
        elementFrom.sendKeys("HRK");
        elementFrom.sendKeys(Keys.ENTER);
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.findElement(By.id("arrivalCity")).click();

        WebElement elementTo = driver.findElement(By.id("citiesSearch"));
        elementFrom.sendKeys("LWO");
        elementFrom.sendKeys(Keys.ENTER);

        driver.findElement(By.xpath("//div[@id='forwardDateItem']/div[2]/div")).click();


//        Set<String> newWindowsSet = driver.getWindowHandles();
//        System.out.println(newWindowsSet);
//
//        newWindowsSet.removeAll(oldWindowsSet);
//        String newWindowHandle = newWindowsSet.iterator().next();
//        System.out.println(newWindowHandle);
//        driver.switchTo().window(newWindowHandle);

      //  driver.findElement(By.id("deprtureCityName")).sendKeys("Харьков");
        return driver;
    }

}
